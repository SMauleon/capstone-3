import { combineReducers, applyMiddleware } from 'redux';
import { configureStore } from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { productDetailsReducer, productsReducer } from './reducers/productReducers';
import { authReducer } from './reducers/userReducers';

const rootReducer = combineReducers({
  products: productsReducer,
  productDetails: productDetailsReducer,
  auth: authReducer
});

const middleware = [thunk];

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(middleware),
  devTools: composeWithDevTools()
});

export default store;
