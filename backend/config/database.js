// Setup Dependencies
const mongoose = require('mongoose')

// MongoDB Connection
const connectDatabase = () => {
    mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.ntfs1sc.mongodb.net/MERN-ecommerce-db?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log("Connected to MongoDB!"))
}

module.exports = connectDatabase