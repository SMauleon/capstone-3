// Setup Dependencies
const Product = require('../models/product');
const dotenv = require('dotenv');
const connectDatabase = require('../config/database');

const products = require('../data/products');

// Setting dotenv file
dotenv.config({path: 'backend/config/config.env'})

connectDatabase();

const seedProducts = async () => {
    try {
        // To refresh product list before adding
        await Product.deleteMany();
        console.log('Products are deleted');

        // Get all the data from products.json file
        await Product.insertMany(products);
        console.log('All products are added.')

        process.exit();

    } catch(error){
        console.log(error.message);
        process.exit();
    }
}

// Call SeedProducts function
seedProducts()