const express = require('express');
const router = express.Router();

const {
    registerUser,
    loginUser,
    logoutUser,
    forgotPassword,
    resetPassword,
    getUserProfile,
    updatePassword,
    updateProfile,

    // Admin
    allUsers,
    getUserDetails,
    updateUser,
    deleteUser
} = require('../controllers/userController');

const {
    isAuthenticatedUser,
    authorizeRoles
} = require('../middlewares/auth')


// Register User
router.route('/register').post(registerUser);

// Login User
router.route('/login').post(loginUser);

// Get User Profile
router.route('/profile').get(isAuthenticatedUser, getUserProfile);

// Update Password
router.route('/password/update').patch(isAuthenticatedUser, updatePassword);

// Update User Profile
router.route('/profile/update').patch(isAuthenticatedUser, updateProfile);

// Forgot Password
router.route('/password/forgot').post(forgotPassword)

// Reset Password
router.route('/password/reset/:token').patch(resetPassword)

// Logout User
router.route('/logout').get(logoutUser);


// ADMIN ROUTES

// Getting All Users (Admin Only)
router.route('/admin/users/all-users').get(isAuthenticatedUser, authorizeRoles('admin'), allUsers);

// Getting User Details (Admin Only)
router.route('/admin/users/details/:id').get(isAuthenticatedUser, authorizeRoles('admin'), getUserDetails);

// Update User Details (Admin Only)
router.route('/admin/users/details/update/:id').patch(isAuthenticatedUser, authorizeRoles('admin'), updateUser);

// Delete User (Admin Only)
router.route('/admin/users/delete/:id').delete(isAuthenticatedUser, authorizeRoles('admin'), deleteUser);

module.exports = router;