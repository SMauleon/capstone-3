// Setup Dependencies
const express = require('express')
const router = express.Router();

const { 
    getAll,
    getProduct, 
    newProduct, 
    updateProduct, 
    deleteProduct, 
    createProductReview,
    getProductReviews,
    deleteReview
} = require('../controllers/productController')

const { isAuthenticatedUser, authorizeRoles } = require('../middlewares/auth')


// Getting All Products
router.route('/products').get(getAll);

// Getting Single Product
router.route('/products/:id').get(getProduct);

// Creating New Product
router.route('/admin/products/new').post(isAuthenticatedUser,  authorizeRoles("admin"), newProduct);

// Update Product
router.route('/admin/products/update/:id').patch(isAuthenticatedUser,  authorizeRoles("admin"), updateProduct);

// Delete Product
router.route('/admin/products/delete/:id').delete(isAuthenticatedUser,  authorizeRoles("admin"), deleteProduct);

// Create Reviews
router.route('/products/review').patch(isAuthenticatedUser, createProductReview)

// Get Reviews of a Product
router.route('/products/reviews/:id').get(isAuthenticatedUser, getProductReviews)

// Delete Product Review
router.route('/products/reviews').delete(isAuthenticatedUser, deleteReview)
                
module.exports = router;