const express = require('express');
const router = express.Router();

const { 
    newOrder,
    getSingleOrder,
    myOrders,
    allOrders,
    updateOrder,
    deleteOrder
 } = require('../controllers/orderController')

const { isAuthenticatedUser, authorizeRoles } = require('../middlewares/auth')

// Create New Order
router.route('/orders/new').post(isAuthenticatedUser, newOrder);

// Get Single Order
router.route('/orders/:id').get(isAuthenticatedUser, getSingleOrder);

// Get Logged In User Orders
router.route('/orders/user-orders/:id').get(isAuthenticatedUser, myOrders);

// Get All Orders (Admin Only)
router.route('/admin/orders/all-orders').get(isAuthenticatedUser, authorizeRoles('admin'), allOrders);

// Update/Process Order (Admin Only)
router.route('/admin/orders/update/:id').patch(isAuthenticatedUser, authorizeRoles('admin'), updateOrder);

// Delete Order (Admin Only)
router.route('/admin/orders/delete/:id').delete(isAuthenticatedUser, authorizeRoles('admin'), deleteOrder);

module.exports = router