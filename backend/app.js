// Setup Dependencies
const express = require('express');
require('dotenv').config() 
const cors = require('cors')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const cloudinary = require('cloudinary').v2
const errorMiddleware = require('./middlewares/errors')
const fileUpload = require('express-fileupload')


// Import all routes
const products = require('./routes/product');
const user = require('./routes/user');
const order = require('./routes/order');



// Server Setup
const app = express();

// Middleware
app.use(cors())
app.use(express.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(cookieParser())
app.use(fileUpload())


// Setting up cloudinary configuration
cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
})


// Routes
app.use('/api/v1', products);
app.use('/api/v1', user);
app.use('/api/v1', order);


// Error Middleware
app.use(errorMiddleware)


module.exports = app;